#!/usr/bin/python3

import os
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from exchange_utils import select_exchange,elapsedtime_calculator
from datetime import datetime
from pytz import timezone




Maintainer = " Javad Nikbakht "
Tehran_tz = timezone('Asia/Tehran')
List_of_exchanges = ['nobitex','exir','dex']


app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from models import Orderbook

@app.route("/get_orderbook/<exchange>")
def add_order_book(exchange):
    print(exchange)
    try:
        # exchange = request.args.get('exchange')
        StartedAt = datetime.now(tz=Tehran_tz)
        FinishedAt = 0
        Status = bool(0)
        EstimatedDuration = 0
        ProcessedDuration = 0
        RowsInserted = 0
        TotalRowsAffected = 0
        RowsUpdated = 0
        RowsDeleted = 0
        ProcessedDuration = 0  
        if exchange not in List_of_exchanges: 
            FinishedAt = datetime.now(tz=Tehran_tz)
            Status = False
            ProcessedDuration = elapsedtime_calculator(StartedAt,FinishedAt)
            EstimatedDuration = 1
            return jsonify({
                        'StartedAt':StartedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                        'FinishedAt':FinishedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                        'Status':Status,
                        'EstimatedDuration':EstimatedDuration,
                        'ProcessedDuration':ProcessedDuration,
                        'RowsInserted':RowsInserted,
                        'TotalRowsAffected':TotalRowsAffected,
                        'RowsUpdated' : RowsUpdated,
                        'RowsDeleted': RowsDeleted,
                        'Maintainer' : Maintainer,
                        'Error' :'We don\'t implement this exchange yet!',
                    })
        else:
            
            for returned_orderbook in select_exchange(exchange):
                orderbook_ts = returned_orderbook.pop('timestamp')
                orderbook = Orderbook(
                    exchange=exchange,
                    bids_asks=str(returned_orderbook),
                    timestamp=orderbook_ts
                )
                db.session.add(orderbook)
                db.session.commit()
                RowsInserted += 1
                TotalRowsAffected += 1
            
            
            Status = True
            FinishedAt = datetime.now(tz=Tehran_tz)
            if exchange == 'exir': 
                EstimatedDuration = 1.5
            elif exchange == 'dex':
                EstimatedDuration = 0.4
            else:
                EstimatedDuration = 0.3
            
            ProcessedDuration = elapsedtime_calculator(StartedAt,FinishedAt) 
            return jsonify({
                'StartedAt':StartedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                'FinishedAt':FinishedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                'Status':Status,
                'EstimatedDuration':EstimatedDuration,
                'ProcessedDuration':ProcessedDuration,
                'RowsInserted':RowsInserted,
                'TotalRowsAffected':TotalRowsAffected,
                'RowsUpdated' : RowsUpdated,
                'RowsDeleted': RowsDeleted,
                'Maintainer' : Maintainer,
                'Message' :'Orderbooks added to database successfully.',
            })
    except Exception as e:
        Status = False
        FinishedAt = datetime.now(tz=Tehran_tz) 
        EstimatedDuration = 1.5
        ProcessedDuration = elapsedtime_calculator(StartedAt,FinishedAt)
        return jsonify({
                'StartedAt':StartedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                'FinishedAt':FinishedAt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                'Status':Status,
                'EstimatedDuration':EstimatedDuration,
                'ProcessedDuration':ProcessedDuration,
                'RowsInserted':RowsInserted,
                'TotalRowsAffected':TotalRowsAffected,
                'RowsUpdated' : RowsUpdated,
                'RowsDeleted': RowsDeleted,
                'Maintainer' : Maintainer,
                'Error' :str(e),
            })


@app.route("/get_all")
def get_all():
    try:
        orderbooks = Orderbook.query.all()
        return jsonify([e.serialize() for e in orderbooks])
    except Exception as e:
        return(str(e))


@app.route("/get/<id_>")
def get_by_id(id_):
    try:
        orderbook = Orderbook.query.filter_by(id=id_).first()
        return jsonify(orderbook.serialize())
    except Exception as e:
        return(str(e))


if __name__ == '__main__':
    app.run()