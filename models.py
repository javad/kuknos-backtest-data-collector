#!/usr/bin/python3

from app import db
from sqlalchemy_utils import JSONType

class Orderbook(db.Model):
    __tablename__ = 'orderbooks'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    exchange = db.Column(db.String(),unique=False, nullable=True)
    bids_asks = db.Column(JSONType,unique=False, nullable=True)    
    timestamp = db.Column(db.Float(),unique=False, nullable=True)

    def __init__(self, exchange, bids_asks, timestamp):
        self.exchange = exchange
        self.bids_asks = bids_asks
        self.timestamp = timestamp

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'exchange': self.exchange,
            'bids_asks': self.bids_asks,
            'timestamp': self.timestamp
        }

