#!/usr/bin/python3

import requests
import time
from datetime import datetime



exir_pairs = ['btc-usdt', 'eth-usdt', 'pmn-usdt']
nobitex_pairs = ['BTCUSDT', 'ETHUSDT', 'PMNUSDT']
dex_pairs = ['PMN-DAYADIAMOND', 'PMN-A101', 'A101-DAYADIAMOND']

dex_currencies = {'A101': {'asset_code': 'A101', 'asset_type': 'credit_alphanum4',
                           'asset_issuer': 'GAZ6ITGO7V3HBWIIEHDYARQPU7PWVQPGMFU4ZTLFA6DE54EARQEO43CK'},
                  'DAYADIAMOND': {'asset_code': 'DAYADIAMOND', 'asset_type': 'credit_alphanum12',
                                  'asset_issuer': 'GBB5AFCTGG73MBQQJ5HZPUDMX7TOZBMXFNAZUHLEOL7BH5FWGYPXIN7N'},
                  'PMN': {'asset_code': 'PMN', 'asset_type': 'native'}}


def nobitex_get_orderbook():
    for item in nobitex_pairs:
        data = {'symbol': item}
        response, orderbook_timestamp = requests.request(
            "POST", "https://api.nobitex.ir/v2/orderbook", data=data), time.time()
        # if response.status_code == 200 and response.json()['status'] == 'ok':
        try:
            json_response = response.json()
            json_response['timestamp'] = int(orderbook_timestamp)
            del json_response['status']
            yield json_response

        except Exception as e:
            yield(str(e))


def exir_get_orderbook():
    for item in exir_pairs:
        params = (
            ('symbol', item),
        )
        response = requests.get(
            'https://api.exir.io/v1/orderbooks', params=params)
        # if response.status_code == 200:
        try:
            json_response = response.json()[item]
            orderbook_timestamp = datetime.strptime(
                json_response['timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ")
            json_response['timestamp'] = int(datetime.timestamp(
                orderbook_timestamp))
            yield json_response

        except Exception as e:
            yield(str(e))


def dex_get_orderbook():
    for pair in dex_pairs:
        selling_asset_code = pair.split("-")[0]
        buying_asset_code = pair.split("-")[1]
        params = (
            ("selling_asset_code", selling_asset_code),
            ("selling_asset_type",
             dex_currencies[selling_asset_code]['asset_type']),
            ("buying_asset_code", buying_asset_code),
            ("buying_asset_type",
             dex_currencies[buying_asset_code]['asset_type']),
        )
        if selling_asset_code != "PMN":
            params = params + \
                (("selling_asset_issuer",
                 dex_currencies[selling_asset_code]['asset_issuer']),)

        if buying_asset_code != "PMN":
            params = params + \
                (("buying_asset_issuer",
                 dex_currencies[buying_asset_code]['asset_issuer']),)

        response, orderbook_timestamp = requests.get(
            "https://horizon.kuknos.org/order_book", params=params), time.time()
        # if response.status_code == 200:
        try:
            json_response = response.json()
            json_response['timestamp'] = int(orderbook_timestamp)
            yield json_response
        except Exception as e:
            yield str(e)


def select_exchange(exchange):
    if exchange == 'nobitex':
        return nobitex_get_orderbook()
    elif exchange == 'exir':
        return exir_get_orderbook()
    elif exchange == 'dex':
        return dex_get_orderbook()


def elapsedtime_calculator(first_time, later_time):
    elapsedTime = later_time - first_time
    return elapsedTime.total_seconds()
