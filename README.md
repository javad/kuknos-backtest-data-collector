Flask Backtest Data Collector
==========================

Getting started
---------------

Clone this repository:

    $ git clone https://salsa.debian.org/javad/kuknos-backtest-data-collector.git

    $ mv kuknos-backtest-data-collector backtest_data_collector


Install requirements (probably in a virtualenv):

    $ cd backtest_data_collector
    $ python3 -m venv venv
    $ . venv/bin/activate
    $ pip install --upgrade pip
    $ pip install -r requirements.txt


Change the configurations to yours

    $ nano .env

Make a Postgres Database:

    $ sudo -u postgres createuser --superuser name_of_user
    $ sudo -u name_of_user createdb name_of_database
    $ psql -U name_of_user -d name_of_database (if you create db successfully this command should work :) )

Export your Setting:
    Now for each using of system you should export its settings: e.g.

    $ export APP_SETTINGS="config.DevelopmentConfig"
    $ DATABASE_URL="postgresql+psycopg2://name_of_user@/name_of_database"

Database Migration

    $ python manage.py db init
    $ python manage.py db migrate
    $ python manage.py db upgrade

Runserver and Use it :)

    $ python manage.py runserver


Testing the API
----------------------

    $ curl http://127.0.0.1:5000/get_orderbook/dex (nobitex,exir)
    $ curl http://127.0.0.1:5000/getall
    $ curl http://127.0.0.1:5000/get/50